# translation of admin.po to
# translation of systems.po to
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# , 2010.
msgid ""
msgstr ""
"Project-Id-Version: admin\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-11-04 15:23+0100\n"
"PO-Revision-Date: 2010-01-28 23:35+0100\n"
"Last-Translator: \n"
"Language-Team: Spanish <>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: html/getfax.php:51
msgid "Could not connect to database server!"
msgstr "¡No puedo conectar al servidor de base de datos!"

#: html/getfax.php:53
msgid "Could not select database!"
msgstr "¡No puedo seleccionar la base de datos.!"

#: html/getfax.php:58 html/getfax.php:62
msgid "Database query failed!"
msgstr "¡La consulta a la base de datos ha fallado.!"

#: gofax/faxreports/detail.tpl:1 gofax/faxreports/contents.tpl:3
msgid "Fax reports"
msgstr "Informes de Fax"

#: gofax/faxreports/detail.tpl:5 gofax/faxreports/detail.tpl:6
msgid "Click on fax to download"
msgstr "Pulse en 'Fax' para descargar"

#: gofax/faxreports/detail.tpl:14
#, fuzzy
msgid "Entry list"
msgstr "Exportar lista"

#: gofax/faxreports/detail.tpl:16
msgid "FAX ID"
msgstr "FAX ID"

#: gofax/faxreports/detail.tpl:20 gofax/faxreports/contents.tpl:37
#: gofax/faxaccount/faxNumberSelect/faxNumberSelect-list.xml:14
#: admin/systems/services/gofax/class_goFaxServer.inc:68
msgid "User"
msgstr "Usuario"

#: gofax/faxreports/detail.tpl:24
msgid "Date / Time"
msgstr "Fecha / Hora"

#: gofax/faxreports/detail.tpl:28 gofax/faxreports/class_faxreport.inc:461
msgid "Sender MSN"
msgstr "MSN del Remitente"

#: gofax/faxreports/detail.tpl:32 gofax/faxreports/class_faxreport.inc:460
msgid "Sender ID"
msgstr "ID del Remitente"

#: gofax/faxreports/detail.tpl:36 gofax/faxreports/class_faxreport.inc:463
msgid "Receiver MSN"
msgstr "MSN del Receptor"

#: gofax/faxreports/detail.tpl:40 gofax/faxreports/class_faxreport.inc:462
msgid "Receiver ID"
msgstr "ID del Receptor"

#: gofax/faxreports/detail.tpl:44 gofax/faxreports/contents.tpl:39
#: gofax/faxreports/class_faxreport.inc:459
msgid "Status"
msgstr "Estado"

#: gofax/faxreports/detail.tpl:48
msgid "Status message"
msgstr "Estado del mensaje"

#: gofax/faxreports/detail.tpl:52 gofax/faxreports/class_faxreport.inc:466
msgid "Transfer time"
msgstr "Tiempo de envío"

#: gofax/faxreports/detail.tpl:56 gofax/faxreports/contents.tpl:42
msgid "# pages"
msgstr "# páginas"

#: gofax/faxreports/contents.tpl:6
msgid "Filter"
msgstr "Filtro"

#: gofax/faxreports/contents.tpl:9 gofax/faxreports/contents.tpl:38
#: gofax/faxreports/class_faxreport.inc:458
msgid "Date"
msgstr "Fecha"

#: gofax/faxreports/contents.tpl:17
msgid "Search for"
msgstr "Buscar por"

#: gofax/faxreports/contents.tpl:19
msgid "Enter user name to search for"
msgstr "Introduzca el nombre de usuario para la búsqueda"

#: gofax/faxreports/contents.tpl:23
msgid "Search"
msgstr "Buscar"

#: gofax/faxreports/contents.tpl:34
#, fuzzy
msgid "Phone reports"
msgstr "Informes de FAX"

#: gofax/faxreports/contents.tpl:40
msgid "Sender"
msgstr "Remitente"

#: gofax/faxreports/contents.tpl:41
msgid "Receiver"
msgstr "Receptor"

#: gofax/faxreports/contents.tpl:60
#, fuzzy
msgid "Page selector"
msgstr "Parametros de grupos"

#: gofax/faxreports/contents.tpl:69
msgid "Search returned no results..."
msgstr "La búsqueda no ha encontrado resultados..."

#: gofax/faxreports/class_faxreport.inc:6
msgid "FAX Reports"
msgstr "Informes de FAX"

#: gofax/faxreports/class_faxreport.inc:7
msgid "View the FAX report or single documents that have been received"
msgstr ""

#: gofax/faxreports/class_faxreport.inc:126
#: gofax/faxreports/class_faxreport.inc:137
#: gofax/faxreports/class_faxreport.inc:142
#: gofax/faxreports/class_faxreport.inc:148
#: gofax/faxreports/class_faxreport.inc:173
#: gofax/faxreports/class_faxreport.inc:279
#: gofax/blocklists/class_blocklistGeneric.inc:109
#: gofax/blocklists/class_blocklistGeneric.inc:170
#: gofax/faxaccount/class_gofaxAccount.inc:254
msgid "Error"
msgstr "Error"

#: gofax/faxreports/class_faxreport.inc:126
#, fuzzy
msgid "No FAX server found!"
msgstr "¡No se ha definido el servidor de FAX!"

#: gofax/faxreports/class_faxreport.inc:129
msgid "Configuration error"
msgstr "Error en la configuración"

#: gofax/faxreports/class_faxreport.inc:130
#, php-format
msgid "Missing %s PHP extension!"
msgstr "¡Extensión PHP %s no encontrada!"

#: gofax/faxreports/class_faxreport.inc:138
#, php-format
msgid "Cannot connect to %s database!"
msgstr "¡No se puede conectar a la base de datos %s!"

#: gofax/faxreports/class_faxreport.inc:143
#, php-format
msgid "Cannot select %s database!"
msgstr "¡No se puede seleccionar la base de datos %s!"

#: gofax/faxreports/class_faxreport.inc:149
#: gofax/faxreports/class_faxreport.inc:174
#: gofax/faxreports/class_faxreport.inc:279
#, php-format
msgid "Cannot query %s database!"
msgstr "¡No se ha podido ejecutar la consulta %s!"

#: gofax/faxreports/class_faxreport.inc:192
msgid "Permission error"
msgstr "Error de permisos"

#: gofax/faxreports/class_faxreport.inc:193
#, fuzzy
msgid "You have no permission to view this FAX id!"
msgstr "¡No tiene permisos para ver este identificador de FAX!"

#: gofax/faxreports/class_faxreport.inc:209
#: gofax/faxreports/class_faxreport.inc:314
msgid "Y-M-D"
msgstr "Y-M-D"

#: gofax/faxreports/class_faxreport.inc:285
msgid "Insufficient permissions to view this attribute"
msgstr "No tiene permisos suficientes para ver este atributo"

#: gofax/faxreports/class_faxreport.inc:441
#: gofax/faxreports/class_faxreport.inc:442
#, fuzzy
msgid "View FAX reports"
msgstr "Informes de Fax"

#: gofax/faxreports/class_faxreport.inc:442
#, fuzzy
msgid "All entries are read-only"
msgstr "Todas las entradas son solo lectura"

#: gofax/faxreports/class_faxreport.inc:456
#, fuzzy
msgid "Detailed view and download"
msgstr "Vista detallada"

#: gofax/faxreports/class_faxreport.inc:457
msgid "Fax ID"
msgstr "Fax ID"

#: gofax/faxreports/class_faxreport.inc:458
msgid "Time"
msgstr "Tiempo"

#: gofax/faxreports/class_faxreport.inc:464
msgid "Number of pages"
msgstr "Número de páginas"

#: gofax/faxreports/class_faxreport.inc:465
msgid "Status Message"
msgstr "Mensaje de estado"

#: gofax/blocklists/blocklist-list.tpl:12 gofax/blocklists/generic.tpl:17
#: gofax/blocklists/class_blocklistGeneric.inc:402
#: gofax/faxaccount/faxNumberSelect/faxNumberSelect-list.tpl:12
msgid "Base"
msgstr "Base"

#: gofax/blocklists/generic.tpl:1 gofax/faxaccount/generic.tpl:8
#: gofax/faxaccount/paste_generic.tpl:6
msgid "Generic"
msgstr "Genérico"

#: gofax/blocklists/generic.tpl:2 gofax/blocklists/blocklist-list.xml:82
#, fuzzy
msgid "Blacklist"
msgstr "Lista de bloqueo"

#: gofax/blocklists/generic.tpl:6
#, fuzzy
msgid "Blacklist generic"
msgstr "Tipo de lista de bloqueo"

#: gofax/blocklists/generic.tpl:8 gofax/blocklists/paste_generic.tpl:3
msgid "List name"
msgstr "Nombre de la lista"

#: gofax/blocklists/generic.tpl:12 gofax/blocklists/paste_generic.tpl:5
#, fuzzy
msgid "Name of blacklist"
msgstr "Nombre de la lista de bloqueos"

#: gofax/blocklists/generic.tpl:32
#: gofax/blocklists/class_blocklistGeneric.inc:405
#, fuzzy
msgid "Blacklist type"
msgstr "Tipo de lista de bloqueo"

#: gofax/blocklists/generic.tpl:34
msgid "Type"
msgstr "Tipo"

#: gofax/blocklists/generic.tpl:37
#, fuzzy
msgid "Select whether to filter incoming or outgoing calls"
msgstr "Seleccione la manera de filtrar llamadas entrantes o salientes"

#: gofax/blocklists/generic.tpl:45 gofax/blocklists/blocklist-list.xml:57
#: gofax/blocklists/class_blocklistGeneric.inc:401
msgid "Description"
msgstr "Descripción"

#: gofax/blocklists/generic.tpl:48
#, fuzzy
msgid "Descriptive text for this blacklist"
msgstr "Descripción de esta lista de bloqueos"

#: gofax/blocklists/generic.tpl:59 gofax/blocklists/generic.tpl:63
msgid "Blocked numbers"
msgstr "Números bloqueados"

#: gofax/blocklists/generic.tpl:87
msgid "Information"
msgstr "Información"

#: gofax/blocklists/generic.tpl:89
msgid "Numbers can also contain wild cards."
msgstr "Los números también pueden contener comodines."

#: gofax/blocklists/blocklist-list.xml:11
#, fuzzy
msgid "List of blacklists"
msgstr "Lista de listas de bloqueos"

#: gofax/blocklists/blocklist-list.xml:15
#: gofax/faxaccount/class_gofaxAccount.inc:831
#, fuzzy
msgid "Send blacklist"
msgstr "Lista de bloqueo salientes"

#: gofax/blocklists/blocklist-list.xml:23
#: gofax/faxaccount/class_gofaxAccount.inc:830
#, fuzzy
msgid "Receive blacklist"
msgstr "Listas de bloqueos entrantes"

#: gofax/blocklists/blocklist-list.xml:49
#: gofax/blocklists/class_blocklistGeneric.inc:212
#: gofax/blocklists/class_blocklistGeneric.inc:215
#: gofax/blocklists/class_blocklistGeneric.inc:222
#: gofax/blocklists/class_blocklistGeneric.inc:400
#: gofax/faxaccount/class_gofaxAccount.inc:103
msgid "Name"
msgstr "Nombre"

#: gofax/blocklists/blocklist-list.xml:65
msgid "Actions"
msgstr "Acciones"

#: gofax/blocklists/blocklist-list.xml:76
msgid "Create"
msgstr "Crear"

#: gofax/blocklists/blocklist-list.xml:94
#: gofax/blocklists/blocklist-list.xml:133 gofax/faxaccount/generic.tpl:138
#: gofax/faxaccount/generic.tpl:147
msgid "Edit"
msgstr "Editar"

#: gofax/blocklists/blocklist-list.xml:101
#: gofax/blocklists/blocklist-list.xml:146
msgid "Remove"
msgstr "Eliminar"

#: gofax/blocklists/paste_generic.tpl:1
#, fuzzy
msgid "Paste blacklist"
msgstr "Lista de bloqueo salientes"

#: gofax/blocklists/class_blocklistManagement.inc:25
#, fuzzy
msgid "FAX Blocklists"
msgstr "Lista de bloqueos de FAX"

#: gofax/blocklists/class_blocklistManagement.inc:26
msgid "Manage FAX number blocklists for incoming and outgoing FAX calls"
msgstr ""

#: gofax/blocklists/class_blocklistGeneric.inc:9
#, fuzzy
msgid "FAX blacklists"
msgstr "Lista de bloqueos de FAX"

#: gofax/blocklists/class_blocklistGeneric.inc:10
#: gofax/faxaccount/class_gofaxAccount.inc:7
msgid "This does something"
msgstr "Esto hace algo"

#: gofax/blocklists/class_blocklistGeneric.inc:109
#: gofax/faxaccount/class_gofaxAccount.inc:254
msgid "Phone number"
msgstr "Número de teléfono"

#: gofax/blocklists/class_blocklistGeneric.inc:152
msgid "send"
msgstr "enviar"

#: gofax/blocklists/class_blocklistGeneric.inc:152
msgid "receive"
msgstr "recibir"

#: gofax/blocklists/class_blocklistGeneric.inc:198
#: gofax/blocklists/class_blocklistGeneric.inc:284
#: gofax/faxaccount/class_gofaxAccount.inc:603
#: gofax/faxaccount/class_gofaxAccount.inc:734
msgid "LDAP error"
msgstr "Error LDAP"

#: gofax/blocklists/class_blocklistGeneric.inc:373
#: gofax/faxaccount/generic.tpl:12 gofax/faxaccount/paste_generic.tpl:10
#: gofax/faxaccount/class_gofaxAccount.inc:809
#: gofax/faxaccount/faxNumberSelect/faxNumberSelect-list.xml:56
msgid "Fax"
msgstr "Fax"

#: gofax/blocklists/class_blocklistGeneric.inc:374
#: gofax/blocklists/class_blocklistGeneric.inc:382
#: gofax/blocklists/class_blocklistGeneric.inc:383
#, fuzzy
msgid "Fax blacklists"
msgstr "Lista de bloqueos de Fax"

#: gofax/blocklists/class_blocklistGeneric.inc:391
msgid "RDN for facsimile blocklist storage."
msgstr ""

#: gofax/blocklists/class_blocklistGeneric.inc:403
#, fuzzy
msgid "Send blacklist entries"
msgstr "Lista de bloqueo salientes"

#: gofax/blocklists/class_blocklistGeneric.inc:404
#, fuzzy
msgid "Receive blacklist entries"
msgstr "Listas de bloqueos entrantes"

#: gofax/blocklists/blocklist-filter.xml:19
#: gofax/faxaccount/faxNumberSelect/faxNumberSelect-filter.xml:21
#, fuzzy
msgid "Default filter"
msgstr "Parámetro"

#: gofax/blocklists/blocklist-filter.xml:33
#, fuzzy
msgid "Receive"
msgstr "Receptor"

#: gofax/blocklists/blocklist-filter.xml:47
#, fuzzy
msgid "Send"
msgstr "Remitente"

#: gofax/faxaccount/generic.tpl:1
#, fuzzy
msgid "Fax account"
msgstr "Mi cuenta"

#: gofax/faxaccount/generic.tpl:10 gofax/faxaccount/paste_generic.tpl:7
#, fuzzy
msgid "Generic settings"
msgstr "Parámetros genéricos del usuario"

#: gofax/faxaccount/generic.tpl:15
msgid "Multiple edit"
msgstr "Edición multiple"

#: gofax/faxaccount/generic.tpl:19 gofax/faxaccount/paste_generic.tpl:15
msgid "Fax number for GOfax to trigger on"
msgstr "Numero de fax para activar GOfax"

#: gofax/faxaccount/generic.tpl:26 gofax/faxaccount/class_gofaxAccount.inc:824
msgid "Language"
msgstr "Idioma"

#: gofax/faxaccount/generic.tpl:31
#, fuzzy
msgid "Specify the GOfax communication language for FAX to mail gateway"
msgstr ""
"Especificar el lenguaje de comunicación de GOfax para la pasarela de fax a "
"correo"

#: gofax/faxaccount/generic.tpl:39 gofax/faxaccount/class_gofaxAccount.inc:825
msgid "Delivery format"
msgstr "Formato de envío"

#: gofax/faxaccount/generic.tpl:43
#, fuzzy
msgid "Specify delivery format for FAX to mail gateway"
msgstr "Especificar el formato de envío para la pasarela de fax a correo"

#: gofax/faxaccount/generic.tpl:58
msgid "Delivery methods"
msgstr "Métodos de envío"

#: gofax/faxaccount/generic.tpl:63
#, fuzzy
msgid "Temporary disable FAX usage"
msgstr "Desactivación temporal del uso del fax"

#: gofax/faxaccount/generic.tpl:69
#, fuzzy
msgid "Deliver FAX as mail to"
msgstr "Enviar fax como correo electrónico a"

#: gofax/faxaccount/generic.tpl:77 gofax/faxaccount/class_gofaxAccount.inc:828
#, fuzzy
msgid "Deliver FAX as mail"
msgstr "Enviar fax como correo electrónico"

#: gofax/faxaccount/generic.tpl:84 gofax/faxaccount/class_gofaxAccount.inc:829
#, fuzzy
msgid "Deliver FAX to printer"
msgstr "Enviar fax a la impresora"

#: gofax/faxaccount/generic.tpl:97
#, fuzzy
msgid "Alternative numbers"
msgstr "Intercambiar los números de fax"

#: gofax/faxaccount/generic.tpl:105
#, fuzzy
msgid "Alternate FAX numbers"
msgstr "Intercambiar los números de fax"

#: gofax/faxaccount/generic.tpl:121
msgid "Add local"
msgstr "Añadir Cuenta Local"

#: gofax/faxaccount/generic.tpl:132 gofax/faxaccount/generic.tpl:133
#, fuzzy
msgid "Blacklists"
msgstr "Listas de bloqueo"

#: gofax/faxaccount/generic.tpl:135
#, fuzzy
msgid "Blacklists for incoming FAX"
msgstr "Listas de bloqueo para fax entrante"

#: gofax/faxaccount/generic.tpl:144
#, fuzzy
msgid "Blacklists for outgoing FAX"
msgstr "Listas de bloqueo para fax saliente"

#: gofax/faxaccount/paste_generic.tpl:1
#, fuzzy
msgid "Paste fax account settings"
msgstr "Configuración cuenta de FAX"

#: gofax/faxaccount/paste_generic.tpl:20
msgid "Alternate fax numbers will not be copied"
msgstr "Los números de fax alternativos no serán copiados"

#: gofax/faxaccount/class_gofaxAccount.inc:6
msgid "FAX"
msgstr "FAX"

#: gofax/faxaccount/class_gofaxAccount.inc:161
#: gofax/faxaccount/class_gofaxAccount.inc:171
#: gofax/faxaccount/class_gofaxAccount.inc:174
msgid "GOfax"
msgstr "GOfax"

#: gofax/faxaccount/class_gofaxAccount.inc:170
#, fuzzy
msgid "Remove FAX account"
msgstr "Eliminar cuenta de fax"

#: gofax/faxaccount/class_gofaxAccount.inc:173
#, fuzzy
msgid "Create FAX account"
msgstr "Crear cuenta de fax"

#: gofax/faxaccount/class_gofaxAccount.inc:394
#: gofax/faxaccount/class_gofaxAccount.inc:477
msgid "back"
msgstr "atrás"

#: gofax/faxaccount/class_gofaxAccount.inc:619
#: gofax/faxaccount/class_gofaxAccount.inc:623
#: gofax/faxaccount/class_gofaxAccount.inc:823
msgid "Fax number"
msgstr "Número de Fax"

#: gofax/faxaccount/class_gofaxAccount.inc:630
msgid "Mail delivery is requested without target address!"
msgstr ""
"¡Ha seleccionado envío de correo, pero no se ha introducido ninguna "
"dirección de correo!"

#: gofax/faxaccount/class_gofaxAccount.inc:632
msgid "Mail address"
msgstr "Dirección correo electrónico"

#: gofax/faxaccount/class_gofaxAccount.inc:632
msgid "your-name@your-domain.com"
msgstr "your-name@your-domain.com"

#: gofax/faxaccount/class_gofaxAccount.inc:638
msgid "Printing is requested without a target printer!"
msgstr ""
"¡Ha seleccionado impresión, pero no se ha introducido ninguna impresora!"

#: gofax/faxaccount/class_gofaxAccount.inc:810
msgid "Fax account settings"
msgstr "Configuración cuenta de FAX"

#: gofax/faxaccount/class_gofaxAccount.inc:826
#, fuzzy
msgid "Alternate FAX number"
msgstr "Intercambiar los números de fax"

#: gofax/faxaccount/class_gofaxAccount.inc:827
#, fuzzy
msgid "Enable/Disable FAX"
msgstr "Activar/desactivar FAX"

#: gofax/faxaccount/faxNumberSelect/faxNumberSelect-list.xml:10
msgid "Please select the desired entries"
msgstr "Por favor seleccione las entradas que desee"

#: gofax/faxaccount/faxNumberSelect/faxNumberSelect-list.xml:40
msgid "Given name"
msgstr "Nombre de pila"

#: gofax/faxaccount/faxNumberSelect/faxNumberSelect-list.xml:48
msgid "Surname"
msgstr "Apellido"

#: gofax/faxaccount/faxNumberSelect/class_faxNumberSelect.inc:28
#, fuzzy
msgid "Fax number selection"
msgstr "Parametros de grupos"

#: gofax/faxaccount/lists.tpl:2
#, fuzzy
msgid "Blocklist settings"
msgstr "Listas de bloqueo"

#: gofax/faxaccount/lists.tpl:7
msgid "Blocked numbers/lists"
msgstr "Listas/Números bloqueados"

#: gofax/faxaccount/lists.tpl:21
#, fuzzy
msgid "List of predefined blacklists"
msgstr "Listas de bloqueos predefinidas"

#: gofax/faxaccount/lists.tpl:22
#, fuzzy
msgid "List of blocked numbers"
msgstr "Números bloqueados"

#: gofax/faxaccount/lists.tpl:29
#, fuzzy
msgid "Add the list to the blacklists"
msgstr "Añadir a la lista de bloqueos"

#: admin/systems/services/gofax/class_goFaxServer.inc:30
msgid "FAX database"
msgstr "Base de datos de FAX"

#: admin/systems/services/gofax/class_goFaxServer.inc:59
msgid "FAX database configuration"
msgstr "Configuración de la base de datos de FAX"

#: admin/systems/services/gofax/class_goFaxServer.inc:71
#: admin/systems/services/gofax/class_goFaxServer.inc:102
#: admin/systems/services/gofax/goFaxServer.tpl:12
msgid "Password"
msgstr "Contraseña"

#: admin/systems/services/gofax/class_goFaxServer.inc:88
#: admin/systems/services/gofax/class_goFaxServer.inc:89
#: admin/systems/services/gofax/goFaxServer.tpl:2
msgid "Fax database"
msgstr "Base de datos de Fax"

#: admin/systems/services/gofax/class_goFaxServer.inc:89
msgid "Services"
msgstr "Servicios"

#: admin/systems/services/gofax/class_goFaxServer.inc:101
msgid "Login name"
msgstr "Nombre de acceso"

#: admin/systems/services/gofax/class_goFaxServer.inc:104
#, fuzzy
msgid "Start"
msgstr "Inicio"

#: admin/systems/services/gofax/class_goFaxServer.inc:105
msgid "Stop"
msgstr ""

#: admin/systems/services/gofax/class_goFaxServer.inc:106
#, fuzzy
msgid "Restart"
msgstr "Recuperar"

#: admin/systems/services/gofax/goFaxServer.tpl:1
msgid "FAX database information"
msgstr "Información de la base de datos del FAX"

#: admin/systems/services/gofax/goFaxServer.tpl:4
msgid "FAX DB user"
msgstr "Base de datos de usuarios de FAX"

#~ msgid "Click the 'Edit' button below to change informations in this dialog"
#~ msgstr ""
#~ "Pulse en el botón - Editar - para cambiar la información en esta ventana"

#~ msgid "FAX settings"
#~ msgstr "Configuración del Fax"

#~ msgid "Alternate fax number"
#~ msgstr "Número de FAX alternativo"

#~ msgid "Deliver fax as mail"
#~ msgstr "Enviar fax como correo electrónico"

#~ msgid "Deliver fax to printer"
#~ msgstr "Enviar fax a la impresora"

#~ msgid "FAX preview - please wait"
#~ msgstr "Visualización del Fax - Por favor espere"

#~ msgid "in"
#~ msgstr "en"

#~ msgid "Select subtree to base search on"
#~ msgstr "Seleccione el subárbol como base de la búsqueda"

#~ msgid "during"
#~ msgstr "durante"

#~ msgid "Insufficient permissions"
#~ msgstr "Permisos insuficientes"

#~ msgid "Fax report"
#~ msgstr "Informe de FAX"

#~ msgid "Warning"
#~ msgstr "Aviso"

#~ msgid ""
#~ "Please double check if you really want to do this since there is no way "
#~ "for GOsa to get your data back."
#~ msgstr ""
#~ "Si está seguro de lo que quiere hacer pulse dos veces, ya que no hay "
#~ "forma de que GOsa pueda recuperar posteriormente esa información"

#~ msgid ""
#~ "So - if you're sure - press 'Delete' to continue or 'Cancel' to abort."
#~ msgstr ""
#~ "Entonces, si esta seguro, presione <i>Eliminar</i> para continuar o "
#~ "<i>Cancelar</i> para Abortar."

#~ msgid "Choose subtree to place group in"
#~ msgstr "Elija el subárbol donde colocar el grupo"

#~ msgid "Select a base"
#~ msgstr "Seleccione una base"

#~ msgid "Blocklist management"
#~ msgstr "Administración de listas de bloqueo"

#~ msgid "Select numbers to add"
#~ msgstr "Seleccione números para añadir"

#~ msgid "Filters"
#~ msgstr "Filtros"

#~ msgid "Display numbers of department"
#~ msgstr "Mostrar número de Departamentos"

#~ msgid "Choose the department the search will be based on"
#~ msgstr "Escoja el departamento base de la búsqueda"

#~ msgid "Display numbers matching"
#~ msgstr "Mostrar números que coincidan con"

#~ msgid "Regular expression for matching numbers"
#~ msgstr "Expresiones regulares que coincidan con números"

#~ msgid "Display numbers of user"
#~ msgstr "Mostrar números de usuario"

#~ msgid "User name of which numbers are shown"
#~ msgstr "Nombre de usuario cuyos números están siendo mostrados"

#~ msgid "Update"
#~ msgstr "Actualizar"

#~ msgid "Submit"
#~ msgstr "Enviar"

#~ msgid ""
#~ "Please double check if your really want to do this since there is no way "
#~ "for GOsa to get your data back."
#~ msgstr ""
#~ "Si está seguro de lo que quiere hacer pulse dos veces, ya que no hay "
#~ "manera de que GOsa recupere posteriormente la información."

#~ msgid "Select all"
#~ msgstr "Seleccione todos"

#~ msgid "Department"
#~ msgstr "Departamento"

#~ msgid "Show send blocklists"
#~ msgstr "Mostrar listas de bloqueos salientes"

#~ msgid "Show receive blocklists"
#~ msgstr "Mostrar listas de bloqueos entrantes"

#~ msgid "Regular expression for matching list names"
#~ msgstr "Expresión regular para encontrar nombres de listas"

#~ msgid "Submit department"
#~ msgstr "Enviar departamento"

#~ msgid "edit"
#~ msgstr "editar"

#~ msgid "Edit user"
#~ msgstr "Editar usuario"

#~ msgid "delete"
#~ msgstr "eliminar"

#~ msgid "Delete user"
#~ msgstr "Eliminar usuario"

#~ msgid "Number of listed '%s'"
#~ msgstr "Número de mostrados '%s'"

#~ msgid "blocklists"
#~ msgstr "listas de bloqueo"

#~ msgid "departments"
#~ msgstr "departamentos"

#~ msgid "Permission"
#~ msgstr "Permisos"

#~ msgid "blocklist"
#~ msgstr "lista de bloqueo"

#, fuzzy
#~ msgid "Copy"
#~ msgstr "copiar"

#, fuzzy
#~ msgid "Cut"
#~ msgstr "mover"

#~ msgid "cut"
#~ msgstr "mover"

#~ msgid "Cut this entry"
#~ msgstr "Mover esta entrada"

#~ msgid "copy"
#~ msgstr "copiar"

#~ msgid "Copy this entry"
#~ msgstr "Copiar esta entrada"

#, fuzzy
#~ msgid "Faxi number"
#~ msgstr "Número de Fax"

#~ msgid "Select to search within subtrees"
#~ msgstr "Seleccione para buscar dentro de los subárboles"

#~ msgid "Search in subtrees"
#~ msgstr "Buscar en subárboles"

#~ msgid "Save"
#~ msgstr "Guardar"

#~ msgid "Cancel"
#~ msgstr "Cancelar"

#~ msgid "The attribute user is empty or contains invalid characters."
#~ msgstr "El atributo usuario está vacío o tiene caracteres no validos"

#~ msgid "The attribute password is empty or contains invalid characters."
#~ msgstr "El atributo contraseña está vacío o tiene caracteres no validos"

#~ msgid "Back"
#~ msgstr "Atrás"

#~ msgid "Add"
#~ msgstr "Añadir"

#~ msgid "Delete"
#~ msgstr "Eliminar"

#~ msgid "Apply"
#~ msgstr "Aplicar"

#~ msgid "Ok"
#~ msgstr "Ok"

#, fuzzy
#~ msgid "Name contains invalid characters!"
#~ msgstr "El atributo usuario está vacío o tiene caracteres no validos"

#~ msgid "Required field 'Name' is not set."
#~ msgstr "No ha introducido el campo obligatorio 'Nombre'."
